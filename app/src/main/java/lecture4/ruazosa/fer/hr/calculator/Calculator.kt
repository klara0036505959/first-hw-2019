package lecture4.ruazosa.fer.hr.calculator

import java.util.*

/**
 * Created by dejannovak on 24/03/2018.
 */
object Calculator {

    var result: Double = 0.0
    private set

    var expression: MutableList<String> = mutableListOf()
    private set

    fun reset() {
        result = 0.0
        expression = mutableListOf()
    }

    fun addNumber(number: String) {
        try {
            val num = number.toDouble()
        } catch (e: NumberFormatException) {
            throw Exception("Not valid number")
        }

        if (expression.count()%2 == 0) {
            expression.add(number)
        }
        else {
            throw Exception("Not a valid order of numbers in expression")
        }
    }

    fun addOperator(operator: String) {
        if (expression.count()%2 != 1)  {
            throw Exception("Not a valid order of operator in expression")
        }
        when (operator) {
            "+" -> expression.add(operator)
            "-" -> expression.add(operator)
            "*" -> expression.add(operator)
            "/" -> expression.add(operator)
            else -> {
                throw Exception("Not a valid operator")
            }
        }
    }

    fun evaluate() {

        if (expression.count() % 2 == 0) {
            throw Exception("Not a valid expression")
        }

        var operatorStack = Stack<String>()
        var operandStack = Stack<Double>()
        var symbol:String

        for(i in 0..expression.count()- 1 step 1) {
            symbol = expression[i]
            if (symbol.equals("*") || symbol.equals("/")){ //biggest priority
                if (!operatorStack.empty() && (operatorStack.peek().equals("*") || operatorStack.peek().equals("/"))){
                    var operand1: Double  = operandStack.pop()
                    var operand2: Double  = operandStack.pop()
                    var operation: String = operatorStack.pop()
                    var res:Double=process(operation,operand1,operand2)
                    operandStack.push(res)
                }

                operatorStack.push(symbol)
            }
            else if (symbol.equals("+") || symbol.equals("-")) {
                if (operatorStack.empty())
                    operatorStack.push(symbol)
                else{
                    while(!operatorStack.empty() &&(operatorStack.peek().equals("*") ||
                            operatorStack.peek().equals("/"))){

                        var operand1: Double  = operandStack.pop()
                        var operand2: Double  = operandStack.pop()
                        var operation: String = operatorStack.pop()
                        var res:Double=process(operation,operand1,operand2)

                        operandStack.push(res)
                    }
                    operatorStack.push(symbol)
                }
            }
            else {
                try {
                    var broj:Double = symbol.toDouble()
                    operandStack.push(broj)
                } catch (e: Exception) {
                    throw Exception("Not valid number")
                }
            }
        }
        while(!operatorStack.empty()){
            var operand1: Double  = operandStack.pop()
            var operand2: Double  = operandStack.pop()
            var operation: String = operatorStack.pop()
            var res:Double=process(operation,operand1,operand2)

            operandStack.push(res)

        }
        result = operandStack.pop()

    }

    fun process(operation: String, operand1: Double, operand2: Double):Double{

        if (operation.equals("*")) {
            return operand1 * operand2
        }
        else if (operation.equals("/")){
            return operand2 / operand1
        }
        else if (operation.equals("+")){
            return operand1 + operand2
        }
        else {
            return operand2 - operand1
        }

    }
}